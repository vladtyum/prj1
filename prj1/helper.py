from django.utils.translation import ugettext_lazy as _

def verbose_choice(*items):
    return [(i, _(i)) for i in items]