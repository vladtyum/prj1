# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cal_calculator', '0003_auto_20151023_0357'),
    ]

    operations = [
        migrations.RenameField(
            model_name='dish',
            old_name='dish_description',
            new_name='description',
        ),
        migrations.RenameField(
            model_name='dish',
            old_name='dish_name',
            new_name='name',
        ),
    ]
