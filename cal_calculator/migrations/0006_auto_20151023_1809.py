# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cal_calculator', '0005_auto_20151023_1804'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='nutrient',
            name='carbs',
        ),
        migrations.RemoveField(
            model_name='nutrient',
            name='fat',
        ),
        migrations.RemoveField(
            model_name='nutrient',
            name='protein',
        ),
    ]
