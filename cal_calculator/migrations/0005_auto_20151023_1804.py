# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cal_calculator', '0004_auto_20151023_1750'),
    ]

    operations = [
        migrations.AddField(
            model_name='nutrient',
            name='carbs',
            field=models.IntegerField(default=0, verbose_name='Carbs'),
        ),
        migrations.AddField(
            model_name='nutrient',
            name='fat',
            field=models.IntegerField(default=0, verbose_name='Fat'),
        ),
        migrations.AddField(
            model_name='nutrient',
            name='protein',
            field=models.IntegerField(default=0, verbose_name='Protein'),
        ),
    ]
