# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Dish',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('dish_name', models.CharField(max_length=50)),
                ('dish_description', models.TextField(blank=True, default='')),
            ],
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('product_name', models.CharField(max_length=50)),
            ],
        ),
        migrations.AddField(
            model_name='dish',
            name='products',
            field=models.ManyToManyField(to='cal_calculator.Product'),
        ),
    ]
