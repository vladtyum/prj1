# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cal_calculator', '0006_auto_20151023_1809'),
    ]

    operations = [
        migrations.AddField(
            model_name='dish',
            name='carbs_sum',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=8),
            preserve_default=False,
        ),
    ]
