# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cal_calculator', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Nutrient',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='NutrInProduct',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('amount', models.DecimalField(max_digits=8, decimal_places=2)),
                ('nutrient', models.ForeignKey(to='cal_calculator.Nutrient')),
                ('product', models.ForeignKey(to='cal_calculator.Product')),
            ],
        ),
        migrations.CreateModel(
            name='PrInDish',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('amount', models.IntegerField(verbose_name='Amount')),
            ],
        ),
        migrations.RemoveField(
            model_name='dish',
            name='products',
        ),
        migrations.AddField(
            model_name='prindish',
            name='dish',
            field=models.ForeignKey(to='cal_calculator.Dish', verbose_name='Dish'),
        ),
        migrations.AddField(
            model_name='prindish',
            name='product',
            field=models.ForeignKey(to='cal_calculator.Product', verbose_name='Product'),
        ),
    ]
