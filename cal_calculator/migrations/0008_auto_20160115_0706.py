# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cal_calculator', '0007_dish_carbs_sum'),
    ]

    operations = [
        migrations.AddField(
            model_name='dish',
            name='chinese_name',
            field=models.CharField(default='名稱', max_length=50),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='product',
            name='chinese_name',
            field=models.CharField(default='名稱', max_length=50),
            preserve_default=False,
        ),
    ]
