from django.contrib import admin
from cal_calculator.models import *

# Register your models here.

# admin.site.register(Dish)
# admin.site.register(Product)
admin.site.register(Nutrient)
# admin.site.register(NutrInProduct)
# admin.site.register(PrInDish)


class DishInLine(admin.TabularInline):
    model = PrInDish
    extra = 2


class DishAdmin(admin.ModelAdmin):
    fields = ['name', 'description', 'carbs_sum']
    inlines = [DishInLine]

admin.site.register(Dish, DishAdmin)

class ProductInLine(admin.TabularInline):
    model = NutrInProduct
    extra = 2


class ProductAdmin(admin.ModelAdmin):
    fields = ['name']
    inlines = [ProductInLine]

admin.site.register(Product, ProductAdmin)