from django.db import models
from django.db.models import Sum
# Create your models here.

class Product(models.Model):
    name = models.CharField(max_length=50)
    chinese_name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

    def sum_by_nutrient(self, nutrient):
        return NutrInProduct.objects.filter(
            product=self,
            nutrient__name=nutrient).aggregate(
            total_sum=Sum('amount'))['total_sum'] or 0


class Dish(models.Model):
    name = models.CharField(max_length=50)
    chinese_name = models.CharField(max_length=50)
    description = models.TextField(blank=True, default = '')
    carbs_sum = models.DecimalField(max_digits=8, decimal_places=2)

    def save(self, *args, **kwargs):
        carbs_array = NutrInProduct.objects.filter(product__prindish__dish=self, nutrient__name='Carbs')
        carbs = 0
        for i in carbs_array:
            carbs += i.amount / 100 * PrInDish.objects.get(dish=self, product__nutrinproduct=i).amount
        self.carbs_sum = carbs
        super(Dish, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.name)

    def sum_by_nutrient(self, nutrient):
        total = 0
        for p in self.prindish_set.filter(product__nutrinproduct__nutrient__name=nutrient):
             total += sum([n.amount / 100 * p.amount for n in p.product.nutrinproduct_set.filter(
                nutrient__name=nutrient)])
        return total


class PrInDish(models.Model):
    product = models.ForeignKey(Product, verbose_name="Product")
    dish = models.ForeignKey(Dish, verbose_name="Dish")
    amount = models.IntegerField(verbose_name="Amount")

    def __str__(self):
        return str(self.dish) + " " + str(self.product)


class Nutrient(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class NutrInProduct(models.Model):
    nutrient = models.ForeignKey(Nutrient)
    product = models.ForeignKey(Product)
    amount = models.DecimalField(max_digits=8, decimal_places=2)

    def __str__(self):
        return str(self.nutrient) + "-" + str(self.product)


