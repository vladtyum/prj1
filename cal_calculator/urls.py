#coding: utf-8
from django.conf.urls import patterns, url

from cal_calculator.views import *


urlpatterns = patterns('',
    url(r'^$', index),
    url(r'^dish/(?P<cal_calculator_dish_id>[0-9]+)/$', dish_view),
    url(r'^product/(?P<cal_calculator_product_id>[0-9]+)/$', product_view),
    # url(r'^blog/page(?P<num>[0-9]+)/$', views.page),
    url(r'^search/', SearchProduct.as_view(), name='search')
)
