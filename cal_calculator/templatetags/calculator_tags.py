from django import template

register = template.Library()

@register.filter
def get_nutrient(obj, count=100):
    return [{'nutrient': nutrient, 
             'value': float(obj.sum_by_nutrient(nutrient)) * float(count / 100.0)}
            for nutrient in ['Protein', 'Carbs', 'Fat']]

@register.simple_tag
def nutrient_from_callories(callories_count, nutrient, percentage=None):
    nutrients = {
        'protein': 4,
        'carb': 4,
        'fat': 9}
    if percentage:
        callories_count = callories_count * percentage
    return round(callories_count / nutrients[nutrient], 2)

@register.filter
def get_calories(obj, count=100):
    calories = {
        'Protein': 4,
        'Carbs': 4,
        'Fat': 9}
    return sum([calories[n['nutrient']] * n['value'] for n in get_nutrient(obj, count)])