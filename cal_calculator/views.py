from django.shortcuts import render
from .models import Dish, Product, NutrInProduct, PrInDish
from django.http import JsonResponse
from django.views.generic import View
from django.db.models import Q
from itertools import chain
from .templatetags.calculator_tags import get_calories
# Create your views here..

def index(request):
    text = {'dishes': Dish.objects.all(),}
    return render(request, "index.html", text)

def product_view(request, cal_calculator_product_id=1 ):
    text = {'Product': Product.objects.get(id=cal_calculator_product_id),
            'Nutrients': NutrInProduct.objects.filter(product_id=cal_calculator_product_id)
            }
    return render(request, "product.html", text)

def dish_view(request, cal_calculator_dish_id=1 ):
    # new code
    dish = Dish.objects.get(id=cal_calculator_dish_id)
    text = {'Dish': dish,
            'Products': dish.prindish_set.all(),
            'Nutrients': NutrInProduct.objects.filter(product__prindish__dish__id=cal_calculator_dish_id),
            'Protein': dish.sum_by_nutrient('Protein'),
            'Carbs': dish.sum_by_nutrient('Carbs'),
            'Fat': dish.sum_by_nutrient('Fat'),
            }
    return render(request, "dish.html", text)


class SearchProduct(View):

    def serialize(self, item):
        obj_info = {
            'id': item.id,
            'name': getattr(item, self.request.LANGUAGE_CODE == 'en-us' and 'name' 
                                                                        or 'chinese_name'),
            'is_product': isinstance(item, Product),
            'nutrients': [('Cal', get_calories(item, 100))],
        }
        for nutrient in ['Protein', 'Carbs', 'Fat']:
            obj_info['nutrients'].append(
                    (nutrient, item.sum_by_nutrient(nutrient)))
  
        return obj_info

    def get(self, request, *args, **kwargs):
        query = request.GET.get('query')
        items = chain(Product.objects.filter(
            Q(name__icontains=query) |
            Q(chinese_name__icontains=query))[:10],
            Dish.objects.filter(
            Q(name__icontains=query) |
            Q(chinese_name__icontains=query))[:10])
        return JsonResponse({'items': [
            self.serialize(item) for item in items]})