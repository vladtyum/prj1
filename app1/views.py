from django.shortcuts import render
from django.http import HttpResponse
# Create your views here.
from django.shortcuts import render_to_response

def index(request):
    view = "LF homepage"
    return render_to_response('home.html', {'name':view})
