from django.contrib import admin

# Register your models here.
from catalog.models import Catalog, Product, CatalogCategory, Nutrient, Ingridient # наша модель из blog/models.py

admin.site.register(Catalog)
admin.site.register(Product)
admin.site.register(CatalogCategory)
admin.site.register(Nutrient)
admin.site.register(Ingridient)