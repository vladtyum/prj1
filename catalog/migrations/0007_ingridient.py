# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0006_nutrient'),
    ]

    operations = [
        migrations.CreateModel(
            name='Ingridient',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('ing_name', models.CharField(max_length=50)),
                ('ing_slug', models.SlugField()),
                ('nutrient', models.ForeignKey(to='catalog.Nutrient')),
            ],
        ),
    ]
