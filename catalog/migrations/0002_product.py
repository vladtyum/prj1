# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('name', models.CharField(max_length=300)),
                ('slug', models.SlugField(max_length=150)),
                ('description', models.TextField()),
                ('photo', models.ImageField(upload_to='product_photo', blank=True)),
                ('manufacturer', models.CharField(max_length=300, blank=True)),
                ('price', models.DecimalField(max_digits=6, decimal_places=2)),
            ],
        ),
    ]
