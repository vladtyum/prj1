# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0005_product_category'),
    ]

    operations = [
        migrations.CreateModel(
            name='Nutrient',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('nutr_name', models.CharField(max_length=50)),
                ('nutr_slug', models.SlugField()),
                ('nutr_type', models.CharField(max_length=9, choices=[('Elm', 'Element'), ('mAacr_Elm', 'MacroElement'), ('vitm', 'vitamins'), ('mIcr_Elm', 'MicroElement')])),
            ],
        ),
    ]
