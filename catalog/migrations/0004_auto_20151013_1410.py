# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0003_catalogcategory'),
    ]

    operations = [
        migrations.AlterField(
            model_name='catalogcategory',
            name='parent',
            field=models.ForeignKey(blank=True, to='catalog.CatalogCategory', related_name='children', null=True),
        ),
    ]
