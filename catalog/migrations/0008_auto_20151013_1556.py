# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0007_ingridient'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='ingridient',
            name='nutrient',
        ),
        migrations.AddField(
            model_name='ingridient',
            name='nutrient',
            field=models.ManyToManyField(to='catalog.Nutrient'),
        ),
    ]
