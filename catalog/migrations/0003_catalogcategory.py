# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0002_product'),
    ]

    operations = [
        migrations.CreateModel(
            name='CatalogCategory',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('name', models.CharField(max_length=300)),
                ('description', models.TextField(blank=True)),
                ('catalog', models.ForeignKey(to='catalog.Catalog')),
                ('parent', models.ForeignKey(blank=True, to='catalog.CatalogCategory', null=True)),
            ],
        ),
    ]
