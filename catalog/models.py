from django.db import models
# Create your models here.
class Catalog(models.Model):
    name = models.CharField(max_length=255)
    slug = models.SlugField(max_length=150)
    publisher = models.CharField(max_length=300)
    description = models.TextField()
    pud_date = models.DateTimeField(auto_now_add=True, blank=True)

    def __str__(self):
        return self.name

# class Product(models.Model):
#     name = models.CharField(max_length=300)
#     slug = models.SlugField(max_length=150)
#     description = models.TextField()
#     photo = models.ImageField(upload_to='product_photo', blank=True)
#     manufacturer = models.CharField(max_length=300, blank=True)
#     price = models.DecimalField(max_digits=6, decimal_places=2)
#
#     def __str__(self):
#         return self.name

class Product(models.Model):
    category = models.ForeignKey('CatalogCategory', related_name='products', blank=True, null=True,)
    name = models.CharField(max_length=300)
    slug = models.SlugField(max_length=150)
    description = models.TextField()
    photo = models.ImageField(upload_to='product_photo', blank=True)
    manufacturer = models.CharField(max_length=300, blank=True)
    price = models.DecimalField(max_digits=6, decimal_places=2)

    def __str__(self):
        return self.name





class CatalogCategory(models.Model):
    catalog = models.ForeignKey('Catalog',)
    parent = models.ForeignKey('self', blank=True, null=True, related_name='children')
    name = models.CharField(max_length=300)
    description = models.TextField(blank=True)

    def __str__(self):
        if self.parent:
            return '%s: %s - %s' %(self.catalog.name, self.parent.name, self.name)
        return '%s: %s' %(self.catalog.name, self.name)


class Nutrient(models.Model):
    NUTRIENT_TYPES = (
        ('Elm', 'Element'),
        ('mAacr_Elm', 'MacroElement'),
        ('vitm', 'vitamins'),
        ('mIcr_Elm', 'MicroElement')
    )

    nutr_name = models.CharField(max_length=50)
    nutr_slug = models.SlugField(max_length=50)
    nutr_type = models.CharField(max_length=9, choices=NUTRIENT_TYPES)

    def __str__(self):
        return self.nutr_name

class Ingridient(models.Model):
    ing_name = models.CharField(max_length=50)
    ing_slug = models.SlugField(max_length=50)
    nutrient = models.ManyToManyField(Nutrient)

