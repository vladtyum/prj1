from django.db import models
from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.utils.translation import ugettext_lazy as _

from prj1.helper import verbose_choice


class UniverseUser(models.Model):
    class Meta:
        abstract = True

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_('user'),
        null=True,
        blank=True)
    anonymous_user = models.CharField(
        _('anonymous_user'),
        max_length=255,
        blank=True,
        null=True)


class BiometricData(UniverseUser):
    class Meta:
        verbose_name = _('user biometrics parameter')
        verbose_name_plural = _('users biometrics parameter')
    
    height = models.SmallIntegerField(_('hight'))
    weight = models.SmallIntegerField(_('weight'))
    sex = models.CharField(
        _('sex'),
        choices=verbose_choice('men',
                               'women'),
        max_length=5)
    age = models.SmallIntegerField(_('age'), default=0)

    @property
    def daily_callories(self):
        s = 5
        if self.sex == 'woman':
            s = -161
        return self.weight * 10 + self.height * 6.25  + self.age * 5 + s


class CalculationItem(UniverseUser):
    class Meta:
        verbose_name = _('calculation item')
        verbose_name_plural = _('calculation items')
    limit = models.Q(app_label = 'cal_calculator', model = 'product') | \
            models.Q(app_label = 'cal_calculator', model = 'dish')
    unique_uuid = models.CharField(_('uuid identifier'), max_length=255)
    content_type = models.ForeignKey(
        ContentType,
        verbose_name=_('content page'),
        limit_choices_to=limit,
        null=True,
        blank=True)
    object_id = models.PositiveIntegerField(
        _('related object'),
        null=True)
    content_object = GenericForeignKey('content_type', 'object_id')
    count = models.SmallIntegerField(verbose_name=_('count'), default=100)
