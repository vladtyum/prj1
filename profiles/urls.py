from django.conf.urls import url
from .views import CreateCalcProduct, CalcProductList, BiometricView

urlpatterns = [
    url(r'^biometric/', BiometricView.as_view(), name='set-biometric'),
    url(r'^choose-products/', CreateCalcProduct.as_view(), name='choose-products'),
    url(r'^set-product-params/', CalcProductList.as_view(), name='set-product-params'),
]
