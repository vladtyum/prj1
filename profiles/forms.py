from django import forms
from .models import BiometricData, CalculationItem
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Div, Layout

class BiometricsForm(forms.ModelForm):

    class Meta:
        model = BiometricData
        fields = ['height', 'weight', 'sex', 'age']


class CalculationItemForm(forms.ModelForm):
    name = forms.CharField()

    class Meta:
        model = CalculationItem
        fields = ['name', 'count', 'content_type', 'object_id']

    def __init__(self, *args, **kwargs):
        super(CalculationItemForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.disable_csrf = True
        self.helper.form_class = 'form-inline'
        self.helper.layout = Layout(
            Div(
                'name',
                'count',
                Div(
                    'content_type',
                    'object_id',
                    css_class='hidden'),
            css_class='form-inline'))


