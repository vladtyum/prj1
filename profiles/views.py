from django.views.generic import FormView, ListView
from django.http import HttpResponseRedirect, JsonResponse, Http404
from django.core.urlresolvers import reverse_lazy
from django.db import models

from django.contrib.contenttypes.models import ContentType
from django.forms import modelformset_factory

from cal_calculator.models import Dish, Product, Nutrient
from cal_calculator.templatetags.calculator_tags import get_nutrient, get_calories

from .models import BiometricData, CalculationItem
from .forms import CalculationItemForm, BiometricsForm
from .mixins import UserMixin, MediaMixin

import uuid


class BiometricView(UserMixin,
                    FormView):
    model = BiometricData
    template_name = 'biometric_params.html'
    success_url = reverse_lazy('set-product-params')
    form_class = BiometricsForm

    def get_object(self):
        biometrics = BiometricData.objects.filter(
            **self.user_data())
        if biometrics.exists():
            return biometrics.first()
        return BiometricData(**self.user_data())

    def get_form_kwargs(self):
        kwargs = super(BiometricView, self).get_form_kwargs()
        if self.get_object().pk:
            kwargs.update({'instance': self.get_object()})
        return kwargs

    def form_valid(self, form):
        form.save(commit=False)
        obj = self.get_object()
        for field in form.cleaned_data:
            setattr(obj, field, form.cleaned_data[field])
        obj.save()
        self.request.session['unique_id']=str(uuid.uuid1())
        return HttpResponseRedirect(self.get_success_url())


class CreateCalcProduct(UserMixin,
                        MediaMixin,
                        ListView):
    model = CalculationItem
    template_name = 'product_choose.html'
    js_files = ['js/product.js', 'js/product.init.js']

    def dispatch(self, request, *args, **kwargs):
        if not request.session.get('unique_id', None):
            request.session['unique_id'] = str(uuid.uuid1())
        return super(CreateCalcProduct, self).dispatch(request, *args, **kwargs)

    def get_item(self):
        models = {
            'product': Product,
            'dish': Dish}
        pk = self.request.POST.get('product')
        model_type = int(self.request.POST.get('is_product')) and 'product' or 'dish'
        return models[model_type].objects.get(pk=pk)

    def get_queryset(self):
        return CalculationItem.objects.filter(
            unique_uuid=self.request.session['unique_id'],
            **self.user_data())

    def post(self, request, *args, **kwargs):
        instance = self.get_item()
        ctype = ContentType.objects.get_for_model(instance)
        obj, created = CalculationItem.objects.get_or_create(
            unique_uuid=self.request.session['unique_id'],
            content_type=ctype,
            object_id=instance.id,
            **self.user_data())
        return JsonResponse({'id': obj.id, 'product': {
                'id': obj.content_object.id
            }})



class CalcProductList(UserMixin,
                      MediaMixin,
                      FormView):
    model = CalculationItem
    template_name = 'set_product_params.html'
    css_files = ['css/main.css']
    js_files = ['js/jquery.formset.js',
                'js/typehead.bundle.min.js',
                'js/init.js']

    # def dispatch(self, request, *args, **kwargs):
    #     if request.session.get('unique_id', None) is None:
    #         raise Http404()
    #     return super(CalcProductList, self).dispatch(request, *args, **kwargs)
    
    # def get_queryset(self):
    #     return CalculationItem.objects.filter(
    #         unique_uuid=self.request.session['unique_id'],
    #         **self.user_data())

    def get_form_class(self):
        return modelformset_factory(CalculationItem, CalculationItemForm, extra=1)

    def get_form_kwargs(self):
        return dict(
            super(CalcProductList, self).get_form_kwargs(),
            queryset=CalculationItem.objects.none())

    def get_biometric(self):
        biometrics = BiometricData.objects.filter(
            **self.user_data())
        if biometrics.exists():
            return biometrics.first()
        return None

    def form_valid(self, formset):
        total = {
            'Protein': 0,
            'Carbs': 0,
            'Fat': 0,
            'cal': 0 }
        other_nutrients = {}
        items = []

        for f in formset:
            obj = {
                'name': f.instance.content_object.name,
                'nutrients': [],
                'cal': ''}

            for n in get_nutrient(f.instance.content_object, f.instance.count):
                obj['nutrients'].append(n)
                total[n['nutrient']] += n['value']

            for n in Nutrient.objects.filter(~models.Q(
                name__in=['Protein', 'Carbs', 'Fat'])):
                value = other_nutrients.get(n.name, 0)
                other_nutrients.update({
                    n.name: value + float(f.instance.content_object.sum_by_nutrient(n.name)) * \
                                    (f.instance.count / 100.) })

            obj['cal'] = get_calories(f.instance.content_object, f.instance.count)
            total['cal'] += obj['cal']
            items.append(obj)
        total_nutrients = sum([total[n] for n in ['Protein', 'Carbs', 'Fat']])
        return self.render_to_response(
            self.get_context_data(
                form=formset,
                items=items,
                other_nutrients=other_nutrients,
                biometric=self.get_biometric(),
                total=[{
                    'name': k,
                    'value': total[k]} for k in ['cal', 'Protein', 'Carbs', 'Fat']],
                proportions=[{
                    'name': n,
                    'value': round((total[n] / total_nutrients) * 100, 2)
                } for n in ['Protein', 'Carbs', 'Fat']]))
