class UserMixin(object):

    def user_data(self):
        data = {
            'user': None,
            'anonymous_user': None
        }
        if self.request.user.is_authenticated():
            data['user'] = self.request.user
        else:
            data['anonymous_user'] = self.request.session.session_key
        return data


class MediaMixin(object):
    js_files = []
    css_files = []

    def get_context_data(self, **kwargs):
        return dict(
            super(MediaMixin, self).get_context_data(**kwargs),
            js_files=self.js_files,
            css_files=self.css_files)