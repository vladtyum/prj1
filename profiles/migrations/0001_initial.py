# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='BiometricData',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('anonymous_user', models.CharField(max_length=255, verbose_name='anonymous_user', null=True, blank=True)),
                ('height', models.SmallIntegerField(verbose_name='hight')),
                ('weight', models.SmallIntegerField(verbose_name='weight')),
                ('sex', models.CharField(max_length=5, choices=[('men', 'men'), ('women', 'women')], verbose_name='sex')),
                ('age', models.SmallIntegerField(default=0, verbose_name='age')),
                ('user', models.ForeignKey(null=True, to=settings.AUTH_USER_MODEL, verbose_name='user', blank=True)),
            ],
            options={
                'verbose_name_plural': 'users biometrics parameter',
                'verbose_name': 'user biometrics parameter',
            },
        ),
        migrations.CreateModel(
            name='CalculationItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('anonymous_user', models.CharField(max_length=255, verbose_name='anonymous_user', null=True, blank=True)),
                ('object_id', models.PositiveIntegerField(null=True, verbose_name='related object')),
                ('count', models.SmallIntegerField(default=100, verbose_name='count')),
                ('content_type', models.ForeignKey(null=True, to='contenttypes.ContentType', verbose_name='content page', blank=True)),
                ('user', models.ForeignKey(null=True, to=settings.AUTH_USER_MODEL, verbose_name='user', blank=True)),
            ],
            options={
                'verbose_name_plural': 'calculation items',
                'verbose_name': 'calculation item',
            },
        ),
    ]
