# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='calculationitem',
            name='unique_uuid',
            field=models.CharField(max_length=255, default='asdsadas', verbose_name='uuid identifier'),
            preserve_default=False,
        ),
    ]
