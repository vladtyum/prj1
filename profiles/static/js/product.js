var FindProduct = (function() {
    var product = $('#find-product');
    var found_products = $('#found_products');
    var choosen_products = $('#choosen_products');

    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    function FindProduct(n) {
        product.on('keyup keypress', this.getProducts.bind(this));
        
        this.filteredProducts = [];
        this.fetchProductList();

    }
    FindProduct.prototype = {
        saveProducts: function(products) {
            this.filteredProducts = products;
        },
        fetchProductList: function() {
            var self = this;

            this.productList = [];
            choosen_products.children().each(function () {
                self.productList.push($(this).find('.product').data('id'));
            });
        },
        addToProductList: function(object, product) {
            if (this.productList.indexOf(object.id) === -1) {
                this.productList.push(object.id);
                choosen_products.append(this.createHTMLProduct(product));
            }            
        },
        getProductById: function(productId, name) {
            var product;
            this.filteredProducts.forEach(function (p) {
              if (p.id == productId && p.name == name) {
                product = p;
              }
            });
            return product;
        },
        bindList: function() {
          $('#found_products li').off().on('click', this.addProduct.bind(this));
        },
        createHTMLProduct: function(product) {
          var nutrients = '';
          product.nutrients.forEach(function(nutrient) {
            nutrients += ['<span class="nutrient">', nutrient[0], '</span>',
                          '<span class="nutrient-value">', nutrient[1], '</span>'].join(' ');
          })
          return ['<li><span data-id=', product.id, ' class="product">',
                  product.name, '</span>', nutrients, '</li>'].join(' ');
        },
        getProducts: function (event) {
          var self = this;
          $.ajax({
              url: '/cal_calculator/search/',
              data: {'query': product.val()}
          }).done(function(results) {
              found_products.empty();
              self.saveProducts(results.items);
              results.items.forEach(function(item) {
                found_products.append($(self.createHTMLProduct(item)));
              });
              self.bindList();
          });
        },
        addProduct: function (event) {
            var self = this,
                item = $(event.target).parent(),
                pr = item.find('.product'),
                product = this.getProductById(pr.data('id'), pr.text().trim());
            $.ajax({
                url: '/choose-products/',
                method: 'post',
                data: {'product': product.id,
                       'is_product': +product.is_product,
                       'csrfmiddlewaretoken': getCookie('csrftoken')}
            }).done(function(result){
                if (result.product.id == product.id) {
                   self.addToProductList(result, product);
                }
            })
            
        }
    };
    return FindProduct;
  })();