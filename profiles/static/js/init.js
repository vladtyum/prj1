$(function() {
  function createTypehead(container) {
      var input = container;
      input.typeahead({
        highlight: true,
        minLenght: 5,
      }, {
        limit: 10,
        source: new Bloodhound({
          datumTokenizer: Bloodhound.tokenizers.whitespace,
          queryTokenizer: Bloodhound.tokenizers.whitespace,

          remote: {
            url: '/choose-products/',
            prepare: function(query, settings) {
              return {
                url: '/cal_calculator/search/',
                data: {
                  query: query
                }
              }
            },
            transform: function(response) {
              return response.items.map(function(item) {
                var nutrients = item.nutrients.map(function(nutrient) {
                  return ['<span class="nutrient">', nutrient[0], '</span>',
                          '<span class="nutrient-value">', nutrient[1], '</span>'].join(' ');
                });

                return {
                  id: item.id,
                  value: item.name, 
                  nutrients: nutrients.join(' '),
                  is_product: item.is_product
                }
              });
            }
          }
        }),
        templates: {
          suggestion: function(item) {
            return $(['<div data-id=', item.id, ' class="item">', item.value, 
                      '<div class="nutrients">', item.nutrients, '</div></div>'].join(' '));
          }
        },
        display: function(item) {
          return item.value;
        }
      });
      input.bind('typeahead:select', function(ev, suggestion) {
        var parent = input.parents('.row.col-md-5'),
            container = parent.find('#nutrients');
        container.empty();
        container.append($(suggestion.nutrients));
        input.val(suggestion.value);
        var content_type = suggestion.is_product ? 'product' : 'dish';
        console.log(content_type);
        parent.find('select[id*="content_type"] option').filter(function() {
          //may want to use $.trim in here
          return $(this).text() == content_type; 
        }).prop('selected', true);
        parent.find('input[id*="object_id"]').val(suggestion.id);
      });
  }
    
  $('.form-good').formset({
      prefix: 'form',
      addCssClass: 'col-md-12 add-item glyphicon glyphicon-plus',
      deleteCssClass: 'col-md-7 remove-item glyphicon glyphicon-minus',
      deleteText: 'remove good',
      addText: 'add good',
      added: function(row) {
          createTypehead($(row).find('input[id*="name"]'));
          $(row).find('#nutrients').empty();
      }
  });
  
  $('input[id*="name"]').each(function(){
    createTypehead($(this));
  });
});